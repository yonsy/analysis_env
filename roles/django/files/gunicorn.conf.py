# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import multiprocessing

bind = '0.0.0.0:8000'
cores = multiprocessing.cpu_count()
workers = cores + 1
loglevel = 'error'
proc_name = 'analysis'
max_requests = 5
timeout = 6000
